# @summary
#   Manage default policies for the internal firewall chains.
#
#   Defaults to accept everything but INPUT and FORWARD.
#
# @example
#   include tirewall::chains
class tirewall::chains {
  $tables = {
    'mangle' => {
      'PREROUTING'  => 'accept',
      'INPUT'       => 'accept',
      'FORWARD'     => 'accept',
      'OUTPUT'      => 'accept',
      'POSTROUTING' => 'accept',
    },
    'raw' => {
      'PREROUTING' => 'accept',
      'OUTPUT'     => 'accept',
    },
    'nat' => {
      'PREROUTING'  => 'accept',
      'INPUT'       => 'accept',
      'OUTPUT'      => 'accept',
      'POSTROUTING' => 'accept',
    },
    'filter' => {
      'INPUT'   => 'drop',
      'FORWARD' => 'drop',
      'OUTPUT'  => 'accept',
    },
  }

  $tables.each | String $table, Hash $chains | {
    $chains.each | String $chain, String $policy | {
      firewallchain { "${chain}:${table}:IPv4":
        ensure => present,
        policy => $policy,
        before => undef,
      }
    }
  }
}
