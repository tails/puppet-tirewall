# @summary
#   Allow trusted subnets to connect to a specific service.
#
# @example
#   tirewall::accept_trusted_subnets { 'My Service':
#     dport => 1234,
#   }
#
# @param dport
#   The destination port(s) that should accept connections from trusted subnets.
define tirewall::accept_trusted_subnets (
  Integer $dport,
) {
  $trusted_subnets = lookup('tirewall::trusted_subnets', Array[Stdlib::IP::Address::V4::CIDR], 'first', [])
  $trusted_subnets.each | Stdlib::IP::Address::V4::CIDR $subnet | {
    firewall { "300 allow trusted subnet ${subnet} to connect to ${name}":
      table  => 'filter',
      chain  => 'INPUT',
      proto  => 'tcp',
      source => $subnet,
      dport  => $dport,
      action => 'accept',
    }
  }
}
