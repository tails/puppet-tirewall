# @summary
#   Configure firewall rules for a public service.
#
#   This resources defines:
#
#       - A local rule accepting all connections in a specific port.
#
#       - An exported rule so our public gateway redirects connections to us
#         (in case there's a gateway configured for our domain).
#
# @example
#   tirewall::public_service { 'My Service':
#     dport        => 8080,
#     public_dport => 8000,
#   }
#
# @param dport
#   The final destination port where the service listens on.
#
# @param public_dport
#   The public destination port where the service listens on. Defaults
#   to $dport.
#
# @param proto
#   The specific protocol to match for this redirect.
define tirewall::public_service (
  Integer $dport,
  Integer $public_dport = $dport,
  Enum['tcp', 'udp'] $proto = 'tcp',
) {
  ### Local firewall rule

  firewall { "300 allow connections to ${name}":
    table  => 'filter',
    chain  => 'INPUT',
    proto  => $proto,
    dport  => $dport,
    action => 'accept',
  }

  ### Remote firewall rule

  $gateways = lookup('tirewall::public_gateways', Hash, 'first', {})

  if $gateways[$::domain] {
    $destination = $gateways[$::domain]['destination']
    $tag = $gateways[$::domain]['tag']

    @@tirewall::redirect { "from ${tag} to ${name} on ${trusted['certname']}":
      destination => $destination,
      proto       => $proto,
      dport       => $public_dport,
      todest      => $facts['ipaddress'],
      toport      => $dport,
      tag         => "target_node:${tag}",
    }
  }
}
