# @summary
#   A firewall redirect.
#
# @param destination
#   The original destination address of the packet.
#
# @param dport
#   The original destination port.
#
# @param todest
#   The new destination address for the packet.
#
# @param toport
#   The new destination port for the packet. Defaults to $dport.
#
# @param proto
#   The protocol of packets that match this rule.
#
# @param iniface
#   The input interface of packets that match this rule.
#
# @param source
#   The source of packets that match this rule.
#
# @example
#   tirewall::redirect { 'to-web-vm':
#     destination => '192.168.122.1',
#     dport       => 80,
#     todest      => '192.168.122.6',
#     toport      => 80,
#   }
define tirewall::redirect (
  String $destination,
  Integer $dport,
  String $todest,
  Integer $toport = $dport,
  Optional[Enum['tcp','udp']] $proto = undef,
  Optional[String] $iniface = undef,
  Optional[String] $source = undef,
) {
  firewall {
    "300 ${name} (redirect NAT)":
      table       => 'nat',
      chain       => 'PREROUTING',
      proto       => $proto,
      iniface     => $iniface,
      source      => $source,
      destination => $destination,
      dport       => $dport,
      jump        => 'DNAT',
      todest      => "${todest}:${toport}";
    "300 ${name} (redirect ACCEPT)":
      chain       => 'FORWARD',
      proto       => $proto,
      source      => $source,
      destination => $todest,
      dport       => $toport,
      action      => 'accept';
  }
}
