# @summary
#   Export firewall rules.
#
#   The `source` param of all rules and redirects exported by this class
#   default to the exporting node's IP address.
#
# @param rules
#   A Hash of rules to be exported.
#   
# @param redirects
#   A Hash of redirects to be exported.
#
# @example
#   include tirewall::export
class tirewall::export (
  Hash $rules = {},
  Hash $redirects = {},
) {
  $defaults = { source => $facts['ipaddress'] }

  $rules.each | String $name, Hash $params | {
    if ! $params['tag'] {
      fail('all exported rules must be tagged to a node.')
    }
    @@tirewall::rule { "${name} (exported from ${trusted['certname']})":
      params => $defaults + $params,  # non-commutative merge, rightmost wins
      tag    => "target_node:${params['tag']}",
    }
  }

  $redirects.each | String $name, Hash $params | {
    if ! $params['tag'] {
      fail('all exported redirects must be tagged to a node.')
    }
    $_params = $params + { 'tag' => "target_node:${params['tag']}" }
    @@tirewall::redirect { "${name} (exported from ${trusted['certname']})":
      *   => $defaults + $_params,  # non-commutative merge, rightmost wins
    }
  }
}
