# @summary
#   Accept forwarding from a source to one or more targets.
#
# @example
#   tirewall::accept_forwarding { 'My Subnet':
#     source  => {
#       subnet => '192.168.122.0/24',
#       iface  => 'virbr0',
#     },
#     targets => {
#       'Some Subnet' => {
#         subnet => '10.10.0.0/24',
#         iface  => 'tun0',
#       },
#       'Some Other Subnet' => {
#         subnet => '10.13.12.0/24',
#         iface  => 'macsec0',
#       },
#     },
#   }
#
# @param source
#   A Hash containing the 'subnet' and 'iface' of the source.
#
# @param targets
#   A Hash mapping target names to a Hash containing their 'subnet' and 'iface' params.
define tirewall::accept_forwarding (
  Hash $source,
  Hash $targets,
) {
  $targets.each | String $target_name, Hash $target_params | {
    firewall {
      "300 accept forwarding from ${title} to ${target_name}":
        table       => 'filter',
        chain       => 'FORWARD',
        source      => $source['subnet'],
        iniface     => $source['iface'],
        proto       => 'all',
        destination => $target_params['subnet'],
        outiface    => $target_params['iface'],
        action      => 'accept';
      "300 accept forwarding from ${target_name} to ${title}":
        table       => 'filter',
        chain       => 'FORWARD',
        source      => $target_params['subnet'],
        iniface     => $target_params['iface'],
        proto       => 'all',
        destination => $source['subnet'],
        outiface    => $source['iface'],
        action      => 'accept';
    }
  }
}
