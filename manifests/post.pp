# @summary
#   Firewall rules added after all others.
#
# @example
#   include tirewall::post
class tirewall::post {
  firewall { '999 drop all':
    ensure => 'present',
    proto  => 'all',
    action => 'drop',
    before => undef,
  }
}
