# @summary
#   Manage a system's firewall.
#
# @example
#   include tirewall
#
# @param purge
#   A Boolean indicating whether to clear any existing rules and make sure that
#   only rules defined in Puppet exist on the machine
#
# @param chainpurge
#   A Boolean indicating whether to clear any existing chains and make sure that
#   only chains defined in Puppet exist on the machine
#   
# @param serverless
#   A Boolean indicating whether this is a serverless node. Exporting and
#   importing rules don't work in such cases.
class tirewall (
  Boolean $purge      = true,
  Boolean $chainpurge = false,
  Boolean $serverless = false,
) {
  include firewall

  resources { 'firewall':
    purge    => $purge,
  }

  resources { 'firewallchain':
    purge  => $chainpurge,
  }

  include tirewall::chains

  # Ensure order of application of rules to avoid being locked out during the
  # first Puppet run.

  include tirewall::pre
  include tirewall::post

  Firewall {
    before   => Class['tirewall::post'],
    require  => Class['tirewall::pre'],
  }

  # Export/import firewall rules

  unless $serverless {
    include tirewall::export
    include tirewall::import
  }
}
