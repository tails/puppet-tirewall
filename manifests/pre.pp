# @summary
#   Firewall rules added before all others.
#
# @example
#   include tirewall::pre
class tirewall::pre {
  Firewall {
    require  => undef,
  }

  # Default firewall rules
  firewall { '001 accept all to lo interface':
    ensure  => 'present',
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }
  -> firewall { '002 reject local traffic not on loopback interface':
    ensure      => 'present',
    iniface     => '! lo',
    proto       => 'all',
    destination => '127.0.0.1/8',
    action      => 'reject',
    provider    => 'iptables',  # needed, otherwise tests fail with "cannot work out protocol family"
  }
  -> firewall { '003 accept incoming traffic caused by outbound connections':
    ensure => 'present',
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }
  -> class { 'tirewall::icmp': }
}
