# @summary
#   Firewall rules for ICMP packets.
#
# @param icmp_accept
#   A Hash mapping ICMP types to codes that will be configured to be accepted
#   by the firewall.
#
# @example
#   include tirewall::icmp
class tirewall::icmp (
  Hash $icmp_accept = {
    'echo-reply'              => 0,
    'destination-unreachable' => 3,
    'echo-request'            => 8,
    'time-exceeded'           => 11,
    'parameter-problem'       => 12,
    'timestamp'               => 13,
    'timestamp-reply'         => 14,
    'traceroute'              => 30,
  },
) {
  $icmp_accept.each | String $type, Integer $code | {
    firewall { "010 accept ICMP ${type}":
      ensure => 'present',
      proto  => 'icmp',
      icmp   => $code,
      action => 'accept',
    }
  }
  firewall { '011 reject ICMP':
    ensure => 'present',
    proto  => 'icmp',
    action => 'reject',
  }
}
