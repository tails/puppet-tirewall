# @summary
#   A firewall rule.
#
# @param params
#   A Hash passed as-is with parameters for configuring a firewall rule.
#
# @example
#   tirewall::rule { '100 my-rule':
#     chain  => 'INPUT',
#     action => 'accept',
#   }
define tirewall::rule (
  Hash $params,
) {
  firewall { $name:
    * => $params,
  }
}
