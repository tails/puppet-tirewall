# @summary
#   Configure firewall for Libvirt:
#
#     - Forward from a bridge subnet to the Internet via the primary network
#       interface.
#
#     - Config is independent of Libvirt, so it's intended to be applied to
#       Libvirt networks configured with: <forward mode='open'/>
#
# @param bridge
#   The bridge to which VMs are connected.
#
# @param subnet
#   The VM subnet.
#
# @example
#   include tirewall::libvirt
class tirewall::libvirt (
  String $bridge                  = 'virbr0',
  Stdlib::IP::Address::V4 $subnet = '192.168.122.0/24',
) {
  # Allow Libvirt guests to use the Internet

  firewall { "100 forward from bridge ${bridge}":
    chain    => 'FORWARD',
    proto    => 'all',
    iniface  => $bridge,
    outiface => $facts["networking"]["primary"],
    action   => 'accept',
  }

  firewall { "100 allow related/established to subnet ${subnet}":
    chain       => 'FORWARD',
    destination => $subnet,
    proto       => 'all',
    ctstate     => ['RELATED', 'ESTABLISHED'],
    action      => 'accept',
    provider    => 'iptables',  # needed, otherwise tests fail with "cannot work out protocol family"
  }

  firewall { "100 SNAT from subnet ${subnet}":
    table    => 'nat',
    proto    => 'all',
    chain    => 'POSTROUTING',
    source   => $subnet,
    outiface => $facts["networking"]["primary"],
    jump     => 'MASQUERADE',
    provider => 'iptables',  # needed, otherwise tests fail with "cannot work out protocol family"
  }

  # Allow Libvirt guests to use services provided by the host

  firewall { "100 allow DHCP from bridge ${bridge}":
    iniface => $bridge,
    sport   => [67, 68],
    dport   => [67, 68],
    proto   => 'udp',
    action  => 'accept',
  }

  firewall { "100 allow DNS inside bridge ${bridge}":
    iniface     => $bridge,
    dport       => 53,
    proto       => 'udp',
    source      => $subnet,
    # Destination should actually be the Libvirt host's own IP in the VM
    # subnet. But, as we don't have that info at hand, it's easier to make this
    # rule a bit less strict by matching the destination using the subnet and
    # assume that it's OK to accept incoming DNS into all the Libvirt host's
    # IPs belonging to this VM subnet.
    destination => $subnet,
    action      => 'accept',
    provider    => 'iptables',  # needed, otherwise tests fail with "cannot work out protocol family"
  }
}
