# @summary
#   Import firewall rules tagged for this node.
#
# @example
#   include tirewall::import
class tirewall::import {
  Tirewall::Rule <<| tag == "target_node:${trusted['certname']}" |>>
  Tirewall::Redirect <<| tag == "target_node:${trusted['certname']}" |>>
}
