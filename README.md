# tirewall

Tirewall is the Tails Firewall module, based on Puppetlabs' firewall module.

## Table of Contents

1. [Description](#description)
1. [Exporting rules and redirects](#exporting-rules-and-redirects)
1. [Public services](#public-services)
1. [Trusted subnets](#trusted-subnets)

## Description

When included, this module configures all basic firewall tables and chains, as
well as a few firewall rules, which result in the following default
configuration:

- Only accept some inbound ICMP (see `tirewall::icmp` for defaults).
- All inbound connections that are unrelated to already established connections
  are dropped.
- Forwarding is rejected.
- Other chains and tables all default to accept. 

By default, this module takes over all firewall configuration of a node and
purges all rules that not defined in Puppet. If you want to prevent that, make
sure to set the `purge` parameter to false when including this class, for
example by adding the following to hiera:

```
tirewall::purge: false
```

## Exporting rules and redirects

It is possible for a node to export rules that will be imported by other nodes.
In that case, rules and redirects can be declared in hiera, and should always
be tagged with the FQDN of the node that should import them:

```
tirewall::rules:
  '001 my rule':
    action: drop
    proto: all
    tag: target_node
tirewall::redirects:
  'my redirect':
    destination: 1.2.3.4
    dport: 8080,
    todest: 5.6.7.8
    toport: 80
    tag: target_node
```

## Public services

To make it easier to configure public services in nodes that don't have public
IPs, we provide a defined resource specific for that. Declaring it will
configure the node to accept connections in a specific port and also makes use
of a Hash describing public gateways to configure redirections from a public
gateway to the node running the service.

Public gateways should be declared in Hiera like so:

```
tirewall::public_gateways:
  domain:
    public_ip: 1.2.3.4/32
    tag: target_node
```

Then, a node can declare a public service like so:

```
tirewall::public_service { 'HTTP':
  dport        => 8080,
  public_dport => 80,
}
```

The node's `$::domain` will be matched against the `tirewall::public_gateways`
hash and a redirection will be configured from the public gateway to the node.
Also, the node will be configured to allow incoming connections in the
service's port.

## Trusted subnets

Another use case that this module supports is allowing connections from a list
of trusted subnets. This is useful, for example, if you have internal
authenticated services (eg. SSH, Icinga2, etc) and thus can be a bit less
restrictive with firewall rules in order to make the whole firewall setup
easier to configure and maintain.

To use this, declare the list of trusted subnets in Hiera:

```
tirewall::trusted_subnets:
  - 1.2.3.4/16
  - 5.6.7.8/24
```

Then, declare that you want to accept all connections from trusted subnets to a
certain port:

```
tirewall::accept_trusted_subnets { 'SSH':
  dport => 22,
}
```
