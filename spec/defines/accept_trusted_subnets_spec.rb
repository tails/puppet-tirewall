# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::accept_trusted_subnets' do
  let(:title) { 'SSH' }
  let(:params) do
    { 'dport' => 22 }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it do
        is_expected.to contain_firewall('300 allow trusted subnet 1.2.3.4/16 to connect to SSH').with(
          'table' => 'filter',
          'chain' => 'INPUT',
          'proto' => 'tcp',
          'source' => '1.2.3.4/16',
          'dport' => 22,
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('300 allow trusted subnet 5.6.7.8/24 to connect to SSH').with(
          'table' => 'filter',
          'chain' => 'INPUT',
          'proto' => 'tcp',
          'source' => '5.6.7.8/24',
          'dport' => 22,
          'action' => 'accept',
        )
      end
    end
  end
end
