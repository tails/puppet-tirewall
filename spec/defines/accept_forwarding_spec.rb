# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::accept_forwarding' do
  let(:title) { 'VMs' }
  let(:params) do
    {
      'source' => {
        'subnet' => '192.168.122.0/24',
        'iface' => 'virbr0',
      },
      'targets' => {
        'VPN' => {
          'subnet' => '10.10.0.0/24',
          'iface' => 'tun0',
        },
        'MACsec' => {
          'subnet' => '10.13.12.0/24',
          'iface' => 'macsec0',
        },
      },
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it do
        is_expected.to contain_firewall('300 accept forwarding from VMs to VPN').with(
          'table' => 'filter',
          'chain' => 'FORWARD',
          'proto' => 'all',
          'source' => '192.168.122.0/24',
          'iniface' => 'virbr0',
          'destination' => '10.10.0.0/24',
          'outiface' => 'tun0',
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('300 accept forwarding from VPN to VMs').with(
          'table' => 'filter',
          'chain' => 'FORWARD',
          'proto' => 'all',
          'source' => '10.10.0.0/24',
          'iniface' => 'tun0',
          'destination' => '192.168.122.0/24',
          'outiface' => 'virbr0',
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('300 accept forwarding from VMs to MACsec').with(
          'table' => 'filter',
          'chain' => 'FORWARD',
          'proto' => 'all',
          'source' => '192.168.122.0/24',
          'iniface' => 'virbr0',
          'destination' => '10.13.12.0/24',
          'outiface' => 'macsec0',
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('300 accept forwarding from MACsec to VMs').with(
          'table' => 'filter',
          'chain' => 'FORWARD',
          'proto' => 'all',
          'source' => '10.13.12.0/24',
          'iniface' => 'macsec0',
          'destination' => '192.168.122.0/24',
          'outiface' => 'virbr0',
          'action' => 'accept',
        )
      end
    end
  end
end
