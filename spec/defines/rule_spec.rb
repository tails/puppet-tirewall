# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::rule' do
  let(:title) { '300 my rule' }
  let(:params) do
    {
      'params' => {
        'destination' => '1.2.3.4/32',
        'action' => 'accept',
      }
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it do
        is_expected.to contain_firewall('300 my rule').with(
          'destination' => '1.2.3.4/32',
          'action' => 'accept',
        )
      end
    end
  end
end
