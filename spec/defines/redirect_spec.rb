# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::redirect' do
  let(:title) { 'my redirect' }
  let(:params) do
    {
      'destination' => '1.2.3.4/32',
      'dport' => 80,
      'todest' => '127.0.0.1',
      'toport' => 8080,
      'proto' => 'tcp',
      'iniface' => 'eth0',
      'source' => '4.5.6.7/32',
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it do
        is_expected.to contain_firewall('300 my redirect (redirect NAT)').with(
          'table' => 'nat',
          'chain' => 'PREROUTING',
          'proto' => 'tcp',
          'iniface' => 'eth0',
          'source' => '4.5.6.7/32',
          'destination' => '1.2.3.4/32',
          'dport' => 80,
          'jump' => 'DNAT',
          'todest' => '127.0.0.1:8080',
        )
      end

      it do
        is_expected.to contain_firewall('300 my redirect (redirect ACCEPT)').with(
          'chain' => 'FORWARD',
          'proto' => 'tcp',
          'source' => '4.5.6.7/32',
          'destination' => '127.0.0.1',
          'dport' => 8080,
          'action' => 'accept',
        )
      end
    end
  end
end
