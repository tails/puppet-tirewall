# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::public_service' do
  let(:title) { 'SSH' }
  let(:params) do
    {
      'dport' => 22,
      'public_dport' => 3004,
      'proto' => 'tcp',
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:node) { 'my_node.domain' }

      it { is_expected.to compile }

      it do
        is_expected.to contain_firewall('300 allow connections to SSH').with(
          'table' => 'filter',
          'chain' => 'INPUT',
          'proto' => 'tcp',
          'dport' => 22,
          'action' => 'accept',
        )
      end

      context 'exported resources' do
        subject { exported_resources }

        it do
          is_expected.to contain_tirewall__redirect('from target_node to SSH on my_node.domain').with(
            'destination' => '1.2.3.4/32',
            'proto' => 'tcp',
            'dport' => 3004,
            'todest' => '172.16.254.254',
            'toport' => 22,
            'tag' => 'target_node:target_node',
          )
        end
      end
    end
  end
end
