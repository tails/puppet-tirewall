# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::libvirt' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge({ 'networking' => { 'primary' => 'eth0' } }) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_firewall('100 forward from bridge virbr0').with(
          'chain' => 'FORWARD',
          'proto' => 'all',
          'iniface' => 'virbr0',
          'outiface' => 'eth0',
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('100 allow related/established to subnet 192.168.122.0/24').with(
          'chain' => 'FORWARD',
          'destination' => '192.168.122.0/24',
          'proto' => 'all',
          'ctstate' => [ 'RELATED', 'ESTABLISHED' ],
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('100 SNAT from subnet 192.168.122.0/24').with(
          'table' => 'nat',
          'proto' => 'all',
          'chain' => 'POSTROUTING',
          'source' => '192.168.122.0/24',
          'outiface' => 'eth0',
          'jump' => 'MASQUERADE',
        )
      end

      it do
        is_expected.to contain_firewall('100 allow DHCP from bridge virbr0').with(
          'iniface' => 'virbr0',
          'sport' => [ 67, 68 ],
          'dport' => [ 67, 68 ],
          'proto' => 'udp',
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('100 allow DNS inside bridge virbr0').with(
          'iniface' => 'virbr0',
          'dport' => [ 53 ],
          'proto' => 'udp',
          'destination' => '192.168.122.0/24',
          'action' => 'accept',
        )
      end
    end
  end
end
