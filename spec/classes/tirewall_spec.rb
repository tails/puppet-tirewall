# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it { is_expected.to contain_class('firewall') }
      it { is_expected.to contain_class('tirewall::chains') }
      it { is_expected.to contain_class('tirewall::pre') }
      it { is_expected.to contain_class('tirewall::post') }
      it { is_expected.to contain_class('tirewall::export') }
      it { is_expected.to contain_class('tirewall::import') }
    end
  end
end
