# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::export' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:node) { 'my_node' }
      let(:params) do
        {
          rules: {
            '001 my rule': {
              'action' => 'drop',
              'proto' => 'all',
              'tag' => 'mynode',
            }
          },
          redirects: {
            'my redirect': {
              'destination' => '1.2.3.4',
              'dport' => 8080,
              'todest' => '5.6.7.8',
              'toport' => 80,
              'proto' => 'tcp',
              'iniface' => 'eth0',
              'tag' => 'mynode',
            }
          },
        }
      end

      it { is_expected.to compile }

      context 'exported resources' do
        subject { exported_resources }

        it do
          is_expected.to contain_tirewall__rule('001 my rule (exported from my_node)').with(
            'params' => {
              'action' => 'drop',
              'proto' => 'all',
              'source' => '172.16.254.254',
              'tag' => 'mynode',
            },
            'tag' => 'target_node:mynode',
          )
        end

        it do
          is_expected.to contain_tirewall__redirect('my redirect (exported from my_node)').with(
            'destination' => '1.2.3.4',
            'dport' => 8080,
            'todest' => '5.6.7.8',
            'toport' => 80,
            'proto' => 'tcp',
            'iniface' => 'eth0',
            'source' => '172.16.254.254',
            'tag' => 'target_node:mynode',
          )
        end
      end
    end
  end
end
