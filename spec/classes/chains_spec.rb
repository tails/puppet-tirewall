# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::chains' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it do
        is_expected.to contain_firewallchain('PREROUTING:mangle:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('INPUT:mangle:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('FORWARD:mangle:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('OUTPUT:mangle:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('POSTROUTING:mangle:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('PREROUTING:raw:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('OUTPUT:raw:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('PREROUTING:nat:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('INPUT:nat:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('OUTPUT:nat:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('POSTROUTING:nat:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewallchain('INPUT:filter:IPv4').with(
          'ensure' => 'present',
          'policy' => 'drop',
        )
      end

      it do
        is_expected.to contain_firewallchain('FORWARD:filter:IPv4').with(
          'ensure' => 'present',
          'policy' => 'drop',
        )
      end

      it do
        is_expected.to contain_firewallchain('OUTPUT:filter:IPv4').with(
          'ensure' => 'present',
          'policy' => 'accept',
        )
      end
    end
  end
end
