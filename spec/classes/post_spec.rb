# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::post' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it do
        is_expected.to contain_firewall('999 drop all').with(
          'ensure' => 'present',
          'proto' => 'all',
          'action' => 'drop',
        )
      end
    end
  end
end
