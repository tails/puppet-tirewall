# frozen_string_literal: true

require 'spec_helper'

describe 'tirewall::pre' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      expected_icmp = {
        'echo-reply'              => 0,
        'destination-unreachable' => 3,
        'echo-request'            => 8,
        'time-exceeded'           => 11,
        'parameter-problem'       => 12,
        'timestamp'               => 13,
        'timestamp-reply'         => 14,
        'traceroute'              => 30,
      }

      expected_icmp.each do |type, code|
        it do
          is_expected.to contain_firewall('010 accept ICMP ' + type).with(
            'ensure' => 'present',
            'proto' => 'icmp',
            'icmp' => code,
            'action' => 'accept',
          )
        end
      end

      it do
        is_expected.to contain_firewall('001 accept all to lo interface').with(
          'ensure' => 'present',
          'proto' => 'all',
          'iniface' => 'lo',
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('002 reject local traffic not on loopback interface').with(
          'ensure' => 'present',
          'proto' => 'all',
          'iniface' => '! lo',
          'action' => 'reject',
        )
      end

      it do
        is_expected.to contain_firewall('003 accept incoming traffic caused by outbound connections').with(
          'ensure' => 'present',
          'proto' => 'all',
          'state' => [ 'RELATED', 'ESTABLISHED' ],
          'action' => 'accept',
        )
      end

      it do
        is_expected.to contain_firewall('011 reject ICMP').with(
          'ensure' => 'present',
          'proto' => 'icmp',
          'action' => 'reject',
        )
      end
    end
  end
end
